import React,{useState} from 'react'
import { View, Text ,TouchableOpacity, Modal, StyleSheet} from 'react-native'
import { useSelector } from 'react-redux'

import OrderItem from './OrderItem';


export default function ViewCart({navigation}) {

    const [modalVisiable,setModalVisible] = useState(false);    
    const {items,restaurantName} = useSelector((state) => state.cartReducers.selectedItems);
    
    const total = items.map((item) => Number(item.price.replace("$", ""))).reduce((prev, curr) => prev + curr, 0);


    console.log("Inside View Cart",items,"The View Cart");

    const totalUSD = total.toLocaleString("en");
    console.log("The Total", total ,"and the totalUSD ",totalUSD)
    const OrderCompleted = () =>{
        setModalVisible(false)
        navigation.navigate("OrderCompleted")
    }


    const checkoutModelContent = () =>{
        return ( 
            <>
            <View style={styles.modalContainer}>
                    <View style={styles.modalCheckoutContainer}>
                        <Text style={styles.restaurantName}>{restaurantName}</Text>
                        {items.map((item,index) =>(
                            <OrderItem key={index} item={item} />
                        ))}
                        <View style={styles.subtotalContainer}>
                            <Text style={styles.subtotalText}  >SubTotal</Text>
                            <Text>{total}</Text>
                        </View>
                        <View
                            style={{flexDirection:"row", justifyContent:"center"}}
                        >
                            <TouchableOpacity
                                style={{
                                    marginTop:20,
                                    backgroundColor:"black",
                                    alignItems:"center",
                                    padding:13,
                                    borderRadius:30,
                                    width:300,
                                    position:"relative"
                                }}
                                onPress={() => OrderCompleted()}
                            >
                                <Text style={{color:"white", fontSize:16}}  >Checkout</Text>
                                <Text style={{position:"absolute", color:"white", right:20,fontSize:16,top:15}}>
                                    {total? totalUSD:""}
                                </Text>
                              
                            </TouchableOpacity>
                        </View>
                    </View>
            </View>
            </>
        )   
    }
    return (
     <>
     <Modal 
        animationType='slide' 
        visible={modalVisiable} 
        transparent={true}
        onRequestClose={() => setModalVisible(false)}
     >
         {checkoutModelContent()}
     </Modal>
     {total ? ( <View style={{
            flex:1,
            alignItems:"center",
            flexDirection:"row",
            position:"absolute",
            bottom:330,
            zIndex:999
        }}>
            <View 
            style={{
                    flexDirection:"row",
                    justifyContent:"center",
                    width:"100%" }}>
                    <TouchableOpacity 
                        style={{
                            marginTop:20,
                            backgroundColor:"black",
                            flexDirection:"row",
                            justifyContent:"space-around",
                            alignItems:"center",
                            padding:13,
                            borderRadius:30,
                            width: 300,
                            position:"relative" 
                        }}
                        onPress={() => setModalVisible(true)}
                    >
                        <Text style={{color:"white", fontSize: 20, marginRight:20}}>
                            View Cart
                        </Text>
                        <Text style={{color:"white", fontSize:20}}>
                            ${totalUSD}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>  ) : (<></>) }
         
     </>
        
       
    )
}

const styles = StyleSheet.create({
    modalContainer:{
        flex: 1,
        justifyContent:"flex-end",
        backgroundColor:"rgba(0,0,0,0.7)"
    },
    modalCheckoutContainer:{
        backgroundColor:"white",
        padding: 16,
        height: 500,
        borderWidth:1,
    },
    restaurantName:{
        textAlign:'center',
        fontWeight:'600',
        fontSize: 18,
        marginBottom: 10,
    },
    subtotalContainer:{
        flexDirection:'row',
        justifyContent:"space-between",
        marginTop: 15
    },
    subtotalText:{
        textAlign:"center",
        fontWeight:"600",
        fontSize :15,
        marginBottom:10,
    }


})
