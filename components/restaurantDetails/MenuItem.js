import React from "react";
import { View, Text, StyleSheet, Image, ScrollView, SafeAreaView } from "react-native";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { Divider } from "react-native-elements";
import { useDispatch , useSelector } from "react-redux";

const styles = StyleSheet.create({
  menuItemStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems:"center",
    marginBottom: 20,
  },

  titleStyle: {
    fontSize: 19,
    fontWeight: "600",
  },
});


export default function MenuItems({
  restaurantName,
  foods,
  hideCheckbox,
  marginLeft,
  
}) {
  const dispatch = useDispatch();
  const selectItem = (item,checkBoxValue ) =>  
    dispatch({
      type : "ADD_TO_CART",
      payload: {...item,
        restaurantName:restaurantName,
        checkBoxValue : checkBoxValue
       
      }
    })
    // {console.log(foods,"Inside MenutiTmes")} 
    const cartItems = useSelector(
      (state) => state.cartReducers.selectedItems.items
    );
  
    const isFoodInCart = (food, cartItems) =>
      Boolean(cartItems.find((item) => item.title === food.title));
  

  return (
    <SafeAreaView >
        <ScrollView contentContainerStyle={{ paddingRight: 100 }  } >
        {foods.map((food, index) => (
                <View  key={index}>
                <View style={styles.menuItemStyle}>
                   { hideCheckbox ? (<></>) :
                   ( <BouncyCheckbox iconStyle={{borderColor:"lightgray", borderRadius:10, border:10}}
                        fillColor="green" 
                        isChecked={isFoodInCart(food, cartItems)}
                        onPress={(checkBoxValue)=> selectItem(food,checkBoxValue)
                    }
                    />)}
                    <FoodInfo food={food} />
                    <FoodImage food={food} marginLeft={marginLeft ? marginLeft : 0} />
                </View>
                <Divider
                    width={0.5}
                    orientation="vertical"
                    style={{ marginHorizontal: 20 }}
                />
                </View>
            ))}
        </ScrollView>
        
    </SafeAreaView>
     
   
  );
}

const FoodInfo = ({marginLeft, ...props}) => (
  <View style={{ width: 240, justifyContent: "space-evenly",marginLeft:marginLeft }}>
    <Text style={styles.titleStyle}>{props.food.title}</Text>
    <Text>{props.food.description}</Text>
    <Text>{props.food.price}</Text>
  </View>
);

const FoodImage = ({ marginLeft, ...props }) => (
  <View>
    <Image
      source={{ uri: props.food.image }}
      style={{
        width: 100,
        height: 100,
        borderRadius: 8,
        marginLeft: marginLeft,
      }}
    />
  </View>
);