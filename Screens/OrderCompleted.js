import React from 'react'
import { SafeAreaView } from 'react-native';
import { View, Text } from 'react-native'
import { useSelector } from 'react-redux'
import LottieView from "lottie-react-native";
import MenuItems from '../components/restaurantDetails/MenuItem';
import { ScrollView } from 'react-native-gesture-handler';

export default function OrderCompleted() {
    const {items,restaurantName} = useSelector((state) => state.cartReducers.selectedItems);
    
    const total = items.map((item) => Number(item.price.replace("$", ""))).reduce((prev, curr) => prev + curr, 0);


    console.log("Inside View Cart",items,"The View Cart");

    const totalUSD = total.toLocaleString("en");
    return (
        <SafeAreaView style={{flex:1, backgroundColor:"white"}} >
            <LottieView
                source={require("../assets/animations/check-mark.json")}
                style={{ height: 100, alignSelf: "center", marginBottom: 30 }}
                autoPlay
                speed={0.5}
                loop={true}
            />
            <Text style={{ fontSize: 20, fontWeight: "bold", color:"black" }}>
                Your order at {restaurantName} has been placed for {total} ok na djkbs jfkesbjf n</Text>
            <ScrollView>
            <View style={{margin:10}}>
            <MenuItems foods={items} hideCheckbox={true} marginLeft={40}/>
                
            </View>    

            </ScrollView>

            <LottieView
                source={require("../assets/animations/cooking.json")}
                style={{ height: 200, alignSelf: "center", marginBottom: 30 }}
                autoPlay
                speed={0.5}
                loop={false}
            />
        </SafeAreaView>
        
    )
}
