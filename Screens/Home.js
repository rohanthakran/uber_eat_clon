import React,{useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text

} from 'react-native';

import { Divider } from 'react-native-elements';


// import BottomTabs from '../components/Home/BottomTabs';
// import Categories from '../components/Home/Categories';
// import HeaderTabs from '../components/HeaderTabs';
// import RestaurantItem, {localResturants} from '../components/Home/RestaurantItem';
// import SearchBar from '../components/Home/SearchBar';
// import BottomTabs from "../components/BottomTabs"
// import Categories from '../components/Categories';
// import HeaderTabs from "../components/HeaderTabs";
// import RestaurantItem,{localResturants} from "../components/RestaurantItem"
// import SearchBar from '../components/SearchBar'

import HeaderTabs from "../components/home/HeaderTabs"
import SearchBar from "../components/home/SearchBar"
import BottomTabs from "../components/home/BottomTabs"
import Categories from "../components/home/Categories"
import RestaurantItem,{localRestaurants} from "../components/home/RestaurantItems"

const Home = ({navigation}) => {
  const [restaurant,setRestaurant] = useState(localRestaurants)
  const [city,setCity] = useState("San Francisco")
  return (

  
   <SafeAreaView style={{backgroundColor:"#eee", flex:1}}>
     <View style={{ backgroundColor:"white" ,padding:15}}>
        <HeaderTabs />
        <SearchBar/>
     </View>
     <ScrollView showsVerticalScrollIndicator={false}>
        <Categories />
        <RestaurantItem restaurantData = {restaurant} navigation={navigation}  />
      </ScrollView>
      <Divider width={1}/>
      <View style={{ backgroundColor:"white"}}>
      <BottomTabs/>
      </View>
   </SafeAreaView>
  );
};

export default Home;
