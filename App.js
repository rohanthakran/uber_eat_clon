import * as React from "react";
import { View,Text } from "react-native";
import Home from "./Screens/Home"
import RestaurantDetails from "./Screens/RestaurantDetails"
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider as ReduxProvider } from "react-redux";
import configureStore from "./redux/store";
import OrderCompleted from "./Screens/OrderCompleted";
const  store = configureStore();


//dispatch will alow us to send and put stuff inside of store
//Use Selector for getting the data from the store
export default function App() {

  const Stack = createNativeStackNavigator();
  const screenOptions = {
    headerShown: false
  }
  return (
  <ReduxProvider store={store}>
      <NavigationContainer>
            <Stack.Navigator initialRouteName="Home" screenOptions={screenOptions}>
              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="RestaurantDetail" component={RestaurantDetails} />
              <Stack.Screen name="OrderCompleted" component={OrderCompleted} />

            </Stack.Navigator>
      </NavigationContainer>  
  </ReduxProvider>
   
  );
}