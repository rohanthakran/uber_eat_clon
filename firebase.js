
import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCzGX8MP9W9uUy_0dA1cnSEVjZCKQUd10A",
    authDomain: "ubereat-17618.firebaseapp.com",
    projectId: "ubereat-17618",
    storageBucket: "ubereat-17618.appspot.com",
    messagingSenderId: "292382827917",
    appId: "1:292382827917:web:a3bab249a385058c2b89c4"
  };

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

export default firebase;