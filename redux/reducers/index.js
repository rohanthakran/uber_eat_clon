import {combineReducers} from "redux"
import cartReducers from "./cartReducers"

let reducers = combineReducers({
    cartReducers: cartReducers
})

const rootreducer= (state,action) =>{
    return reducers(state,action);
}

export default rootreducer